﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Drawing;


namespace altitudes_map_wpf
{
    /// <summary>
    /// Логика взаимодействия для map.xaml
    /// </summary>
    public partial class Map : Window
    {
        private static readonly string[] Separators = {" ", "\r\n" }; // разделители для вычленения цифр из файла
        private const float RgbMax = 255; // максимальное значение параметров rgb
        private const int MapWidth = 100; // ширина рисунка определеная эмпирически, почему так понятия не имею

        public Map()
        {
            InitializeComponent();
        }

        public void mapDraw(string path)
        {
            int[] altitudeArray = System.IO.File.ReadAllText(path).Split(Separators, StringSplitOptions.RemoveEmptyEntries).Select(n => int.Parse(n)).ToArray(); // грузим файл и парсим его в массив

            float colorModif = RgbMax / altitudeArray.Max(); //высчитываем коэфициент для определения цвета высоты

            Bitmap map = new Bitmap(MapWidth, altitudeArray.Length/MapWidth+1); // подгон как он есть

            int xCoordinate = 0; int yCoordinate = 0; int altitudeColor;

            foreach (int altitude in altitudeArray)
            {
                if (xCoordinate > MapWidth - 1) // что-то вроде #10#13
                {
                    xCoordinate = 0;
                    yCoordinate++;
                }

                altitudeColor = Convert.ToInt32(altitude * colorModif); // считаем цвет пикселя(чем выше тем светлее)

                map.SetPixel(xCoordinate, yCoordinate, Color.FromArgb(altitudeColor, altitudeColor, altitudeColor)); // закрашиваем пиксель

                xCoordinate++;
            }
            mapPicture.Source=bitmapToImageConvert(map); 
        }


        private BitmapImage bitmapToImageConvert(Bitmap map) //image в wpf не понимает bitmap, поэтому костыль
        {
            MemoryStream memory = new MemoryStream();
            map.Save(memory, ImageFormat.Png);

            memory.Position = 0;

            BitmapImage mapConverted = new BitmapImage();
            mapConverted.BeginInit();
            mapConverted.StreamSource = memory;
            mapConverted.CacheOption = BitmapCacheOption.OnLoad;
            mapConverted.EndInit();

            return mapConverted;
        }
    }
}
