﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
namespace altitudes_map_wpf
{
    /// <summary>
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void getPathBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog atlPathDialog = new Microsoft.Win32.OpenFileDialog();

            atlPathDialog.Multiselect = false;
            atlPathDialog.Filter = "text files(*.txt)|*.txt";
            atlPathDialog.Title = "Select text file";
      
            if (atlPathDialog.ShowDialog()==true) // довольно криво
                pathTextBox.Text = atlPathDialog.FileName.ToString();

            Map mapForm= new Map();
            mapForm.Owner = this; // чтобы при закрытии этой формы закрылась и вызванная
            mapForm.Show();
            mapForm.mapDraw(pathTextBox.Text);
            
        }
    }
}
